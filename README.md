HTTP API tests
==============

This is a standard procedure for testing HTTP APIs.

For who:
--------

Developers or QA engineers who have to write automated tests for their HTTP APIs

Why:
----

Standard way of testing HTTP APIs across all services
 - reduces time needed to learn multiple test frameworks
 - saves time spent on evaluating different options for testing
 - improves reliability of your tests by deeper understanding of quirks in one specific testing stack


Running these tests on the developer machine:
---------------------------------------------

Requirements:

 - Docker
 - docker-compose


```bash
cd my-service
npm install
cd ../my-service-api-tests
npm install
cd ..
docker-compose run my-service-api-tests
docker-compose stop
```

Adding tests to your project
----------------------------

### Assumptions:


Service under test is built as a Docker container. It can be run using docker-compose.


### Procedure:

1. Include code from example in `my-service-api-tests` into the repository of your service
2. Integrate the tests into your `docker-compose.yml`. Make sure to link the container with tests and service under test.
3. Adjust address of the service in `my-service-api-tests/tests/config.ts`. Use service names and ports as defined in `docker-compose.yml`.
4. Run `docker-compose build`
5. Run `docker-compose run my-service-api-tests` whenever you want to run them.


Full story behind this repository:
https://drola.si/post/2019-02-19-testing-http-apis/
module.exports = {
	moduleFileExtensions: [
		'ts',
		'js'
	],
	transform: {
		'^.+\\.(ts|tsx)$': 'ts-jest'
	},
	testMatch: [
		'**/tests/**/*.spec.(ts|js)'
	],
	testEnvironment: 'node'
};

import request = require('supertest');
import {BASE_URL} from './config';

describe('POST /news', () => {
    it('Should save and return back the news', async() => {
        const response = await request(BASE_URL)
            .post('/news')
            .send({'title': 'Breaking news!'})
            .expect(201);

        expect(response.body).toEqual({'id': 1, 'title': 'Breaking news!'});
    });
});

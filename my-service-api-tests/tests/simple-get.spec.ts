import request = require('supertest');
import {BASE_URL} from './config';

describe('Hello world', () => {
    it('Should say "hello world"', async() => {
        const response = await request(BASE_URL)
            .get('/')
            .expect(200);
        expect(response.text).toBe("hello world");
    });
});

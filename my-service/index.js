var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());

// GET /
app.get('/', function (req, res) {
  res.send('hello world')
});

// POST /news
app.post('/news', function(req, res) {
  let body = req.body;
  body.id = 1;
  res.status(201).send(body);
});

console.log("KRNEKI");

var port = 3000;
app.listen(port, () => console.log(`Example app listening on port ${port}!`));